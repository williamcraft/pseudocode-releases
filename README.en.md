# pseudocode-releases

#### Description
This is the release repository of pseudocode runtime, lsp, and updater implementation. Download the releases from [here](https://gitee.com/williamcraft/pseudocode-releases/releases).

I'm not sure if I will open-source the project yet. Frankly, I want to use this project in my PS so I don't really want people to plagiarise my code lol.
